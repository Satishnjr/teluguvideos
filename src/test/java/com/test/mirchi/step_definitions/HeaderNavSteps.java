package com.test.mirchi.step_definitions;

import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.test.mirchi.helpers.DataHelper;
import com.test.mirchi.modules.HeaderNavAction;
import com.test.mirchi.pageobjects.HomePage;

import cucumber.api.java.en.Then;

public class HeaderNavSteps {

	public WebDriver driver;
	public List<HashMap<String, String>> datamap;

	public HeaderNavSteps() {
		driver = Hooks.driver;
	}

	@Then("^I Verify it shows valid videos$")
	public void i_Verify_it_shows_valid_videos() throws Throwable {
		PageFactory.initElements(driver, HomePage.class);
		HeaderNavAction.checkNoVideos(driver);
	}

}
