package com.test.mirchi.step_definitions;

import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.test.mirchi.helpers.DataHelper;
import com.test.mirchi.modules.HomePageAction;
import com.test.mirchi.pageobjects.HomePage;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class HomePageSteps {
	public WebDriver driver;
	public List<HashMap<String, String>> datamap;

	public HomePageSteps() {
		driver = Hooks.driver;
	}

	@When("^I open Telugu Videos website$")
	public void i_open_Telugu_Videos_website() throws Throwable {
		driver.get("http://www.myteluguvideos.com/");
	}

	@When("^I click on Nav Link$")
	public void i_click_on_NavLink() throws Throwable {
		PageFactory.initElements(driver, HomePage.class);
		HomePageAction.health(driver);
	}

	@When("^I click on Nav Link in mobile$")
	public void i_click_on_NavLink_In_Mobile() throws Throwable {
		driver.manage().window().setSize(new Dimension(412, 732));
		PageFactory.initElements(driver, HomePage.class);
		HomePageAction.mobile(driver);
	}

	@When("^I click on Music Nav Link$")
	public void i_click_on_Music_NavLink() throws Throwable {
		PageFactory.initElements(driver, HomePage.class);
		HomePageAction.music(driver);
	}

	@When("^I click on Movies Nav Link$")
	public void i_click_on_Movies_Nav_Link() throws Throwable {
		PageFactory.initElements(driver, HomePage.class);
		HomePageAction.movies(driver);
	}

	@When("^I click on Trailers Nav Link$")
	public void i_click_on_Trailers_Nav_Link() throws Throwable {
		PageFactory.initElements(driver, HomePage.class);
		HomePageAction.trailer(driver);
	}
	
	@Then("^I click on Video$")
	public void i_click_on_Video() throws Throwable {
		PageFactory.initElements(driver, HomePage.class);
		HomePageAction.video(driver);
	}

	@When("^I click on Video, Video Should Play$")
	public void i_click_on_Video_Video_Should_Play() throws Throwable {
		PageFactory.initElements(driver, HomePage.class);
		HomePageAction.video(driver);
		HomePageAction.videoPlay(driver);
	}

	@Then("^I click on Overview, Should go for Home Page$")
	public void i_click_on_Overview_Should_go_for_Home_Page() throws Throwable {
		PageFactory.initElements(driver, HomePage.class);
		HomePageAction.overviewLink(driver);
	}

}
