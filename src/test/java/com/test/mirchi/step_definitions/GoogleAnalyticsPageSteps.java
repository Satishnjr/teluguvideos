package com.test.mirchi.step_definitions;

import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import cucumber.api.java.en.Then;

public class GoogleAnalyticsPageSteps {
	public WebDriver driver;
	public List<HashMap<String, String>> datamap;

	public GoogleAnalyticsPageSteps() {
		driver = Hooks.driver;
	}

	@Then("^I validate page source,Google Analytics code should be present$")
	public void i_validate_page_source_Google_Analytics_code_should_be_present()
			throws Throwable {

		String pagesource = driver.getPageSource();
		// System.out.println(pagesource);
		Assert.assertTrue(pagesource.contains("www.googletagmanager.com/gtag/js"));
		

	}
}

	
