Feature: Telugu Videos Site

  Scenario: Select Navigation
    When I open Telugu Videos website
    And I click on Nav Link
    Then I Verify it shows valid videos

  Scenario: Select Navigation in mobile
    When I open Telugu Videos website
    And I click on Nav Link in mobile
    Then I Verify it shows valid videos

  Scenario: Select Navigation for Music
    When I open Telugu Videos website
    And I click on Music Nav Link
    Then I Verify it shows valid videos

  Scenario: Select Navigation for Movies
    When I open Telugu Videos website
    And I click on Movies Nav Link
    Then I Verify it shows valid videos

  Scenario: Select Navigation for Trailer
    When I open Telugu Videos website
    And I click on Trailers Nav Link
    Then I Verify it shows valid videos

  
