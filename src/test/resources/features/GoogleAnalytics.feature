Feature: Google Analytics code check

  Scenario: Check for Google Analytics code in Page Source
    Given I open Telugu Videos website
    Then I validate page source,Google Analytics code should be present

  Scenario: Select Movies Navigation and Check for Google Analytics
    Given I open Telugu Videos website
    And I click on Movies Nav Link
    Then I validate page source,Google Analytics code should be present

  Scenario: Select Music Navigation and Check for Google Analytics
    Given I open Telugu Videos website
    And I click on Music Nav Link
    Then I validate page source,Google Analytics code should be present

  Scenario: Select Video in Trailers Navigation and Check for Google Analytics code
    Given I open Telugu Videos website
    When I click on Trailers Nav Link
    And I click on Video
    Then I validate page source,Google Analytics code should be present

 