Feature: Videos in Telugu Videos Site

  Scenario: Select Video in trailer nav
    When I open Telugu Videos website
    And I click on Trailers Nav Link
    Then I click on Video

  Scenario: Select Video and land on home page
    When I open Telugu Videos website
    And I click on Trailers Nav Link
    And I click on Video, Video Should Play
    Then I click on Overview, Should go for Home Page
